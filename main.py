#!/usr/bin/env python3


""" HTPL: A Programmer's approach to HTML. """


import os
import sys
import getopt

# program = 'start html section head title "Hello" end head end html'.split()

fname = "boring.htpl"

f = open(fname, 'r')

program = f.read().split()
#print(program)

fn = os.path.splitext(fname)

of = fn[0] + ".html"

out_file = open(of, "w")

for p, op in enumerate(program):
    # print(program)
    if program[p] == "start":
        #print("I saw a start tag")
        if program[p+1] == 'html':
            out_file.write("<!DOCTYPE html>\n")
            out_file.write("<html>\n")
    if program[p] == "section":
        #print("I saw a section tag")
        if program[p+1] == "head":
            out_file.write("    <head>\n")
        if program[p+1] == "body":
            out_file.write("    <body>\n")
    if program[p] == "title":
        #print("I saw a title tag")
        if program[p+1] == 'end' or program[p+1] == 'h1' or program[p+1] == 'h2' or program[p+1] == 'h3' or program[p+1] == 'h4' or program[p+1] == 'h5' or program[p+1] == 'h6' or program[p+1] == 'p':
            pass
        else:
            title_rep = program[p+1].replace('"', '')
            tr = title_rep.replace('_', ' ')
            out_file.write(f"       <title>\n\t{tr}\n")
    if program[p] == "h1":
        #print("I saw a header tag.")
        if program[p+1] == 'end' or program[p+1] == 'h1' or program[p+1] == 'h2' or program[p+1] == 'h3' or program[p+1] == 'h4' or program[p+1] == 'h5' or program[p+1] == 'h6' or program[p+1] == 'p':
            pass
        else:
            header_rep = program[p+1].replace('"', '').replace("_", " ")
            out_file.write(f"       <h1>\n\t{header_rep}\n")
    if program[p] == "h2":
        #print("I saw a header tag.")
        if program[p+1] == 'end' or program[p+1] == 'h1' or program[p+1] == 'h2' or program[p+1] == 'h3' or program[p+1] == 'h4' or program[p+1] == 'h5' or program[p+1] == 'h6' or program[p+1] == 'p':
            pass
        else:
            header2_rep = program[p+1].replace('"', '').replace("_", " ")
            out_file.write(f"       <h2>\n\t{header2_rep}\n")
    if program[p] == "h3":
        #print("I saw a header tag.")
        if program[p+1] == 'end' or program[p+1] == 'h1' or program[p+1] == 'h2' or program[p+1] == 'h3' or program[p+1] == 'h4' or program[p+1] == 'h5' or program[p+1] == 'h6' or program[p+1] == 'p':
            pass
        else:
            header3_rep = program[p+1].replace('"', '').replace("_", " ")
            out_file.write(f"       <h3>\n\t{header3_rep}\n")
    if program[p] == "h4":
        #print("I saw a header tag.")
        if program[p+1] == 'end' or program[p+1] == 'h1' or program[p+1] == 'h2' or program[p+1] == 'h3' or program[p+1] == 'h4' or program[p+1] == 'h5' or program[p+1] == 'h6' or program[p+1] == 'p':
            pass
        else:
            header4_rep = program[p+1].replace('"', '').replace("_", " ")
            out_file.write(f"       <h4>\n\t{header4_rep}\n")
    if program[p] == "h5" :
        #print("I saw a header tag.")
        if program[p+1] == 'end' or program[p+1] == 'h1' or program[p+1] == 'h2' or program[p+1] == 'h3' or program[p+1] == 'h4' or program[p+1] == 'h5' or program[p+1] == 'h6' or program[p+1] == 'p':
            pass
        else:
            header5_rep = program[p+1].replace('"', '').replace("_", " ")
            out_file.write(f"       <h5>\n\t{header5_rep}\n")
    if program[p] == "h6":
        #print("I saw a header tag.")
        if program[p+1] == 'end' or program[p+1] == 'h1' or program[p+1] == 'h2' or program[p+1] == 'h3' or program[p+1] == 'h4' or program[p+1] == 'h5' or program[p+1] == 'h6' or program[p+1] == 'p':
            pass
        else:
            header6_rep = program[p+1].replace('"', '').replace("_", " ")
            out_file.write(f"       <h6>\n\t{header6_rep}\n")
    if program[p] == "p":
        if program[p+1] == 'end' or program[p+1] == 'h1' or program[p+1] == 'h2' or program[p+1] == 'h3' or program[p+1] == 'h4' or program[p+1] == 'h5' or program[p+1] == 'h6' or program[p+1] == 'p':
            pass
        else:
            p_rep = program[p+1].replace('"', '').replace("_", " ")
            out_file.write(f"       <p>\n\t{p_rep}\n")
    if program[p] == "css":
        if program[p+1] == 'end' or program[p+1] == 'h1' or program[p+1] == 'h2' or program[p+1] == 'h3' or program[p+1] == 'h4' or program[p+1] == 'h5' or program[p+1] == 'h6' or program[p+1] == 'p':
            pass
        else:
            css_rep = program[p+1].replace('"', '')
            out_file.write(f'       <link rel="stylesheet" href="{css_rep}">\n')
    if program[p] == "end":
        #print("I saw an end tag")
        if program[p+1] == "p":
            out_file.write("       </p>\n")
        if program[p+1] == "h1":
            out_file.write("       </h1>\n")
        if program[p+1] == "h2":
            out_file.write("       </h2>\n")
        if program[p+1] == "h3":
            out_file.write("       </h3>\n")
        if program[p+1] == "h4":
            out_file.write("       </h4>\n")
        if program[p+1] == "h5":
            out_file.write("       </h5>\n")
        if program[p+1] == "h6":
            out_file.write("       </h6>\n")
        if program[p+1] == "title":
            #print("title end tag here.")
            out_file.write("       </title>\n")
        if program[p+1] == "head":
            out_file.write("    </head>\n")
        if program[p+1] == "body":
            out_file.write("    </body>\n")
        if program[p+1] == "html":
            out_file.write("</html>\n")
