# HTPL
A programmer's approach to HTML

## Why HTPL?
- I want to make a simple programming language that translates to HTML.

## Quick Start
```console
$ ./build.sh
```

## Hello World in HTPL
```
start html
	section body
		h1 "Hello_World!!" end h1
	end body
end html
```
